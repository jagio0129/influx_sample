# frozen_string_literal: true

require 'spec_helper'
require 'open3'
require_relative '../../lib/client.rb'

describe 'InfluxDB::Client' do
  # InfluxDB settings
  let(:url) { 'http://localhost:8086' }
  let(:username) { 'root' }
  let(:password) { 'password' }
  let(:org) { 'hoge-org' }
  let(:database) { 'test_db' }
  let(:bucket) { "#{database}/autogen" }

  let(:client) { InfluxDB::Client.new(url, username, password, bucket, measurement, org) }
  let(:now) { Time.now }

  # rspecを中断するとテストデータが残ってしまうので最初に全削除
  before do
    call_delete_measurement_api(database, measurement)
  end

  # example終了ごとに削除
  after do
    call_delete_measurement_api(database, measurement)
  end

  describe 'queries' do
    let(:measurement) { 'mem' }
    let(:tags) { { host: 'host1' } }
    let(:field) { { used_percent: 23.4324543 } }

    before do
      call_write_api(url, bucket, post_data('another_mem', tags, field))
    end

    context '#all' do
      it 'データを２つ投入したらレコードが２つ取得できること' do
        call_write_api(url, bucket, post_data(measurement, tags, field, (now - 1).to_i))
        call_write_api(url, bucket, post_data(measurement, tags, field, now.to_i))

        expect(client.all.count).to eq 2
      end

      context 'データの内容があっていること' do
        before do
          call_write_api(url, bucket, post_data(measurement, tags, field, now.to_i))
        end

        it 'measurement' do
          expect(client.all.last.values['_measurement']).to eq measurement
        end

        it 'field' do
          expect(client.all.last.values['_field']).to eq field.keys.first.to_s
        end

        it 'value' do
          expect(client.all.last.values['_value']).to eq field.values.first
        end

        it 'time' do
          expect(client.all.last.values['_time']).to eq now.to_datetime.rfc3339
        end
      end
    end
  end

  def call_write_api(url, bucket, post_data)
    o, e, s = Open3.capture3("curl -X POST '#{url}/api/v2/write?bucket=#{bucket}&precision=ns' --data-raw \'#{post_data}\'")
  end

  def call_query_api(url, _bukcet, username, password, measurement)
    o, e, s = Open3.capture3("
      curl -XPOST '#{url}/api/v2/query' -sS \
      -H 'Accept:application/csv' \
      -H 'Content-type:application/vnd.flux' \
      -H 'Authorization: Token #{username}:#{password}' \
      --data-raw 'from(bucket:\"#{bucket}\")
        |> range(start:-5m)
        |> filter(fn:(r) => r._measurement == \"#{measurement}\")'
      ")
  end

  def call_delete_measurement_api(database, measurement)
    o, e, s = Open3.capture3("
      curl -XPOST '#{url}/query?db=#{database}' --data-raw 'q=delete from #{measurement}'
    ")
  end

  def post_data(measurement, tags, field, time = Time.now.to_i)
    raise 'tags is not Hash' unless tags.is_a?(Hash)

    tags_ary = tags.map { |key, value| "#{key}=#{value}" }
    "#{measurement},#{tags_ary.join(' ')} #{field.keys.first}=#{field.values.first} #{time.to_s << '000000000'}"
  end
end
