influxdb2
===
dockerで触ってみる

```
docker run --name influxdb -p 9999:9999 quay.io/influxdb/influxdb:2.0.0-beta
```
デフォルトではInfluxDataにデータが送信される仕組みなっている。送信をさせない場合は`--reporting-disabled`オプションを付けて建てる

## InfluxData
https://www.influxdata.com/telemetry/

InfluxDBの利用状況に関する情報を収集している。「テレフォンホーム」という機能でユーザコミュニティに関する情報の提供とプラットフォームがどのように使用されているかを見る目的。

統計情報は他人に共有されない。InfluxData社に送っているよう。
同社の方針として、収集を嫌がるユーザがいることも理解しているので、情報を送信しない方法も提供している。

InfluxDB 2.xをインストールごとに以下リンクの項目を収集する。その後は8時間ごとに収集
https://www.influxdata.com/telemetry/

## Set up InfluxDB through
UI(localhost:9999)とCLIどちらからでも初期設定できる

## write
```
influx write \
  -b bucketName \
  -o orgName \
  -p s \
  'myMeasurement,host=myHost testField="testData" 1556896326'
```

csvからでも投入できる
```
influx write \
  -b bucketName \
  -o orgName \
  -p s \
  --format=csv
  -f /path/to/data.csv
```

curlでAPIを叩いて書き込みできる
```
curl -XPOST "http://localhost:9999/api/v2/write?org=YOUR_ORG&bucket=YOUR_BUCKET&precision=s" \
  --header "Authorization: Token YOURAUTHTOKEN" \
  --data-raw "
mem,host=host1 used_percent=23.43234543 1556896326
mem,host=host2 used_percent=26.81522361 1556896326
mem,host=host1 used_percent=22.52984738 1556896336
mem,host=host2 used_percent=27.18294630 1556896336
"
```

## その他
- Snowflake
  - DWH as a service
- Data WareHouse
  - 時系列に整理されたデータを保持するシステム
  - https://aws.amazon.com/jp/data-warehouse/
