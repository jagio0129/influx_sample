Get started with wiith InfluxDB OSS
===

- [公式ドキュメント](https://docs.influxdata.com/influxdb/v1.8/introduction/get-started/)
- v1.8
- CLIはデフォルトではポート8086を介して通信を行う

## Creating a database

```
# influx login
influx

# create database
> create database <DB_NAME>

# show database
> show databases
name: databases
name
----
test_db
_internal
test

# select database
> user test
```

## Writing and exloiring data

- influxdbではゼロ以上のpointsと呼ばれるデータを持つ
- pointsは以下のパラメータを持つ
  - time
    - タイムスタンプ
    - 主キーの役割
  - measurement
    - テーブル名のようなもの
  - field
    - テーブルのカラムのようなもの
    - key-valueで持つ実データ
  - tag
    - テーブルのカラムのようなもの
    - indexの役割
- measurementは何百万でも持てる
- スキーマをテー技する必要がない
- null値は保存されない
- pointsは以下のような構造
  - `<measurement>[,<tag-key>=<tag-value>...] <field-key>=<field-value>[,<field2-key>=<field2-value>...] [unix-nano-timestamp]`
```e.g.
cpu,host=serverA,region=us_west value=0.64
payment,device=mobile,product=Notepad,method=credit billed=33,licenses=3i 1434067467100293230
stock,symbol=AAPL bid=127.46,ask=127.48
temperature,machine=unit42,type=assembly external=25,internal=37 1434067467000000000
```

- pointsの挿入はinsertで行える
```
INSERT cpu,host=serverA,region=us_west value=0.64
```

- measurementがcpu, tagsがhost=serverA,region=us_westで値が0.64

