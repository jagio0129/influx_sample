# frozen_string_literal: true

require 'influxdb-client'

class InfluxDB
  class Client
    def initialize(url, username, password, bucket, measurement, org = '-')
      @bucket = bucket
      @measurement = measurement
      @client = InfluxDB2::Client.new(url,
                                      "#{username}:#{password}",
                                      bucket: bucket,
                                      org: org, # InfluxDB 1.xでは無視される
                                      use_ssl: false,
                                      precision: InfluxDB2::WritePrecision::NANOSECOND)
    end

    def read(query)
      @client.create_query_api.query(query: query)
    end

    def all
      query = "from(bucket: \"#{@bucket}\")
        |> range(start: 1970-01-01T00:00:00.000000001Z)
        |> filter(fn:(r) => r._measurement == \"#{@measurement}\")
      "
      read(query)[0].records
    end

    def write(point)
      @client.create_write_api(write_options: @write_options).write(data: point)
    end

    def point(measurement, _tags, _fields, time = Time.now)
      InfluxDB2::Point.new(name: measurement)
                      .add_tag('host', 'host1')
                      .add_field('used_percent', 21.43234543)
                      .time(time, InfluxDB2::WritePrecision::NANOSECOND)
    end
  end
end
