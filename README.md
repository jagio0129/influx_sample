# InfluxDB with Ruby

use [influxdata/influxdb-client-ruby](https://github.com/influxdata/influxdb-client-ruby)

## How to login to InfluxDB 1.x
```
docker exec -it <container_id> bash
influx
```

