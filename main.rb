# frozen_string_literal: true

require 'influxdb2/client'
require 'pry'

url = 'http://localhost:8086'
username = 'test'
password = 'test'
database = 'test_db'
retention_policy = 'autogen'

bucket = "#{database}/#{retention_policy}"

client = InfluxDB2::Client.new(url,
                               "#{username}:#{password}",
                               bucket: bucket,
                               org: '-',
                               use_ssl: false,
                               precision: InfluxDB2::WritePrecision::NANOSECOND)

# write
write_api = client.create_write_api
point = InfluxDB2::Point.new(name: 'mem')
                        .add_tag('host', 'host1')
                        .add_field('used_percent', 21.43234543)
                        .time(Time.now, InfluxDB2::WritePrecision::NANOSECOND)
puts point.to_line_protocol
write_api.write(data: point)

# read
query_api = client.create_query_api
query = "from(bucket: \"#{bucket}\") |> range(start: -1h)"
result = query_api.query(query: query)
result[0].records.each { |record| puts "#{record.time} #{record.measurement}: #{record.field} #{record.value}" }

client.close!
